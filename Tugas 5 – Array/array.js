// Soal No. 1 (Range)
console.log("\nsoal 1\n======")

function range(min, max){
   var numbers 
    if(!min || !max){
        numbers = -1;
    }else if(min <= max){
        numbers = new Array();
        for(var i = min; i <= max; i++){
            numbers.push(i);
        }
    }else if(min > max){
        numbers = new Array();
        for(var i = min; i >= max; i--){
            numbers.push(i);
        }
    }
    return numbers;
} 

console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 

//Soal No. 2 (Range with Step)
console.log("\nsoal 2\n======")

function rangeWithStep(min, max, step){
    var numbers 
    if(!min || !max || !step){
        numbers = -1;
    }else if(min <= max){
        numbers = new Array();
        for(var i = min; i <= max; i = i + step){
            numbers.push(i);
        }
    }else if(min > max){
        numbers = new Array();
        for(var i = min; i >= max; i = i - step){
            numbers.push(i);
        }
    }
    return numbers;
}


console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 

//Soal No. 3 (Sum of Range)
console.log("\nsoal 3\n======")

function sum(min, max, step){
    var numbers 
    if(!step){
        step = 1;
    }

    if (!max){
        max = 0;
    }

    if (!min){
        min = 0;
    }

    if(min <= max){
        numbers = new Array();
        for(var i = min; i <= max; i = i + step){
            numbers.push(i);
        }
    }else if(min > max){
        numbers = new Array();
        for(var i = min; i >= max; i = i - step){
            numbers.push(i);
        }
    }
    return numbers.reduce(jumlahin);
}

function jumlahin(total, num) {
  return total + num;
}

console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 


//Soal No. 4 (Array Multidimensi)
console.log("\nsoal 4\n======");

function dataHandling(isi){
    for(var i = 0 ; i < isi.length ; i++ ){
        console.log("Nomor ID:  " + isi[i][0]);
        console.log("Nama Lengkap:  " + isi[i][1]);
        console.log("TTL:  " + isi[i][2]);
        console.log("Hobi:  " + isi[i][3]);
        console.log("\n");
    }
}

var input = [
                ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
                ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
                ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
                ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
            ] 

dataHandling(input);


//Soal No. 5 (Balik Kata)
console.log("\nsoal 5\n======")

function balikKata(text){
    var index = 0;
    var hasil = "";
    for(var i = text.length ; i > 0 ; i--){
        index = i-1;
        hasil += text[index];
    }
    return hasil;
}

console.log(balikKata("Kasur Rusak")); // kasuR rusaK
console.log(balikKata("SanberCode")); // edoCrebnaS
console.log(balikKata("Haji Ijah")); // hajI ijaH
console.log(balikKata("racecar")); // racecar
console.log(balikKata("I am Sanbers")); // srebnaS ma I 


//Soal No. 6 (Metode Array)
console.log("\nsoal 6\n======")

function dataHandling2(isi){
    isi.splice(1, 1, "Roman Alamsyah Elsharawy") ;
    isi.splice(2, 1, "Provinsi Bandar Lampung") ;
    isi.splice(4, 1, "Pria") ;
    isi.splice(6, 0, "SMA Internasional Metro") ;
    console.log(isi)
    
    var tanggal_lahir = isi[3].split("/");

    switch(parseInt(tanggal_lahir[1])) {
        case 1: { console.log('Januari'); break; }
        case 2: { console.log('Februari'); break; }
        case 3: { console.log('Maret'); break; }
        case 4: { console.log('April'); break; }
        case 5: { console.log('Mei'); break; }
        case 6: { console.log('Juni'); break; }  
        case 7: { console.log('Juli'); break; }
        case 8: { console.log('Agustus'); break; }
        case 9: { console.log('September'); break; }
        case 10: { console.log('Oktober'); break; }
        case 11: { console.log('November'); break; }
        case 12: { console.log('Desember'); break; }
        default : { console.log('Inputan bulan salah!'); }
    }

    var tanggal_lahir_join = tanggal_lahir.join("-");
    var tanggal_lahir_sort = tanggal_lahir.sort(function(a, b){return b-a}) ;
    console.log(tanggal_lahir_sort);
    console.log(tanggal_lahir_join);
    
    var nama_slice = isi[1].slice(0,15);
    console.log(nama_slice);
    
    
}

var input = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2(input);