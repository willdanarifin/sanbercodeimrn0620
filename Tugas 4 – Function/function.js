// No. 1

function teriak(){
    var teriakan = "Halo Sanbers!";
    return teriakan;
}

console.log(teriak())


// No. 2
console.log("\n");

function kalikan(bil1, bil2){
    var hasil = bil1 * bil2;
    return hasil;
}

var num1 = 12;
var num2 = 4;
 
var hasilKali = kalikan(num1, num2);
console.log(hasilKali);


// No. 3
console.log("\n");

function introduce(nama, umur, alamat, hobi){
    var kenalkan = "Nama saya "+ nama +", umur saya "+ umur +" tahun, alamat saya di "+ alamat +", dan saya punya hobby yaitu "+ hobby +"! ";
    return kenalkan;
}
 
var name = "Agus"
var age = 30
var address = "Jln. Malioboro, Yogyakarta"
var hobby = "Gaming"
 
var perkenalan = introduce(name, age, address, hobby)
console.log(perkenalan)