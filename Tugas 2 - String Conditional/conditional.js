// soal if-else

function ifelse(nama, peran){
    if(nama == "" && peran == ""){
        console.log("Nama harus diisi!");
    }else if (nama != "" && peran == ""){
        console.log("Halo "+ nama +", Pilih peranmu untuk memulai game!");
    }else if (nama != "" && peran == "Penyihir") {
        console.log("Selamat datang di Dunia Werewolf, " + nama);
        console.log("Halo Penyihir  "+ nama +", kamu dapat melihat siapa yang menjadi werewolf!");
    }else if (nama != "" && peran == "Guard") {
        console.log("Selamat datang di Dunia Werewolf, " + nama);
        console.log("Halo Guard "+ nama +", kamu akan membantu melindungi temanmu dari serangan werewolf.");
    }else if (nama != "" && peran == "Warewolf") {
        console.log("Selamat datang di Dunia Werewolf, " + nama);
        console.log("Halo Werewolf  "+ nama +", Kamu akan memakan mangsa setiap malam!" );
    }else{
        console.log("Isi data dengan benar!");
    }
}

console.log("IF ELSE 1\n===============");
ifelse("","");console.log("\n");

console.log("IF ELSE 2\n===============");
ifelse("John","");console.log("\n");

console.log("IF ELSE 3\n===============");
ifelse("Jane","Penyihir");console.log("\n");

console.log("IF ELSE 4\n===============");
ifelse("Jenita","Guard");console.log("\n");

console.log("IF ELSE 5\n===============");
ifelse("Junaedi","Warewolf");console.log("\n");



console.log("\n")
//soal switch-case

var hari = 21; 
var bulan = 12; 
var tahun = 1945;

switch(true){
    case (hari < 1 || hari > 31):{
        console.log('Inputan tanggal salah!');
        break;
    }case (tahun < 1900 || tahun > 2200):{
        console.log('Inputan tahun salah!');
        break;
    }default :{
        switch(bulan) {
            case 1: { console.log(hari + ' Januari ' + tahun); break; }
            case 2: { console.log(hari + ' Februari ' + tahun); break; }
            case 3: { console.log(hari + ' Maret ' + tahun); break; }
            case 4: { console.log(hari + ' April ' + tahun); break; }
            case 5: { console.log(hari + ' Mei ' + tahun); break; }
            case 6: { console.log(hari + ' Juni ' + tahun); break; }  
            case 7: { console.log(hari + ' Juli ' + tahun); break; }
            case 8: { console.log(hari + ' Agustus ' + tahun); break; }
            case 9: { console.log(hari + ' September ' + tahun); break; }
            case 10: { console.log(hari + ' Oktober ' + tahun); break; }
            case 11: { console.log(hari + ' November ' + tahun); break; }
            case 12: { console.log(hari + ' Desember ' + tahun); break; }
            default : { console.log('Inputan bulan salah!'); }
        }
    }
}