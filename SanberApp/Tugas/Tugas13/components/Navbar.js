import React, {Component} from 'react';
import {Text, StyleSheet, View, Image, TouchableOpacity} from 'react-native';
import BackIcon from '../assets/back_icon.png';

export default class Navbar extends Component {
  render() {
    return (
      <View style={styles.container}>
        <TouchableOpacity style={styles.iconWrapper}>
          <Image source={BackIcon} />
        </TouchableOpacity>
        <View style={styles.titleWrapper}>
          <Text> {this.props.title} </Text>
        </View>
        <View style={{height: 25, width: 25}} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    height: 45,
    backgroundColor: '#FFFFFF',
  },
  iconWrapper: {
    justifyContent: 'center',
  },
  titleWrapper: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
