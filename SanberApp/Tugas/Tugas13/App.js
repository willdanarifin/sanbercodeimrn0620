import React, {Component} from 'react';
import {Text, StyleSheet, View} from 'react-native';
import Login from './Login';
import AboutMe from './AboutMe';
import Skills from './Skills';

export default class App extends Component {
  render() {
    return (
      <View style={styles.container}>
         <Login /> 
         {/* <AboutMe /> 
        <Skills /> */}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
