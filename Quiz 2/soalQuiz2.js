/**
 * Berikut soal quiz kali ini, terdiri dari 3 Soal
 * Kerjakan dengan sebaik mungkin, dengan menggunakan metode yang telah dipelajari,
 * Tidak diperkenankan untuk menjawab hanya dengan console.log('teks jawaban');
 * maupun dengan terlebih dahulu memasukkannya ke dalam variabel, misal var a = 'teks jawaban'; console.log(a);
 * 
 * Terdapat tambahan poin pada setiap soal yang dikerjakan menggunakan sintaks ES6 (+5 poin)
 * Jika total nilai Anda melebihi 100 (nilai pilihan ganda + coding), tetap akan memiliki nilai akhir yaitu 100
 * 
 * Selamat mengerjakan
*/

/*========================================== 
  1. SOAL CLASS SCORE (10 poin + 5 Poin ES6)
  ==========================================
  Buatlah sebuah class dengan nama Score. class Score tersebut memiliki properti "subject", "points", dan "email". 
  "points" dapat di input berupa number (1 nilai) atau array of number (banyak nilai).
  tambahkan method average untuk menghitung rata-rata dari parameter points ketika yang di input berupa array (lebih dari 1 nilai)
*/

class Score {
  constructor(subject, points, email) {
        
        this.subject = subject;
        this.points = points;
        this.email = email;
    }
    average() {
        const arrAvg = arr => arr.reduce((a, b) => (a + b)) / arr.length;
        if(Array.isArray(this.points) == true){
            return arrAvg(this.points);
        }else{
            return this.points;
        }
    }
}

//var n = new Score("subject", [10, 11, 12], "willdan.aprizal15@gmail.com");
//console.log(n.average())

/*=========================================== 
  2. SOAL Create Score (10 Poin + 5 Poin ES6)
  ===========================================
  Membuat function viewScores yang menerima parameter data berupa array multidimensi dan subject berupa string
  Function viewScores mengolah data email dan nilai skor pada parameter array 
  lalu mengembalikan data array yang berisi object yang dibuat dari class Score.
  Contoh: 

  Input
   
  data : 
  [
    ["email", "quiz-1", "quiz-2", "quiz-3"],
    ["abduh@mail.com", 78, 89, 90],
    ["khairun@mail.com", 95, 85, 88]
  ]
  subject: "quiz-1"

  Output 
  [
    {email: "abduh@mail.com", subject: "quiz-1", points: 78},
    {email: "khairun@mail.com", subject: "quiz-1", points: 95},
  ]
*/

const data = [
  ["email", "quiz-1", "quiz-2", "quiz-3"],
  ["abduh@mail.com", 78, 89, 90],
  ["khairun@mail.com", 95, 85, 88],
  ["bondra@mail.com", 70, 75, 78],
  ["regi@mail.com", 91, 89, 93]
]

var viewScores = (data, subject) => {
    var output = [];

    for(var i = 0; i < data.length  ; i++){
        if(data[0][i] == subject){
            for(var j = 1; j < data.length; j++){

                var nilai = new Score(subject, data[j][i], data[j][0]);
                var listdata = {};
                listdata.email = nilai.email;
                listdata.subject = nilai.subject;
                listdata.points = nilai.points;
                output.push(listdata);
            }
        }
    } 
    console.log(output);
}


// TEST CASE
viewScores(data, "quiz-1")
viewScores(data, "quiz-2")
viewScores(data, "quiz-3")


console.log("\n")
/*==========================================
  3. SOAL Recap Score (15 Poin + 5 Poin ES6)
  ==========================================
    Buatlah fungsi recapScore yang menampilkan perolehan nilai semua student. 
    Data yang ditampilkan adalah email user, nilai rata-rata, dan predikat kelulusan. 
    predikat kelulusan ditentukan dari aturan berikut:
    nilai > 70 "participant"
    nilai > 80 "graduate"
    nilai > 90 "honour"

    output:
    1. Email: abduh@mail.com
    Rata-rata: 85.7
    Predikat: graduate

    2. Email: khairun@mail.com
    Rata-rata: 89.3
    Predikat: graduate

    3. Email: bondra@mail.com
    Rata-rata: 74.3
    Predikat: participant

    4. Email: regi@mail.com
    Rata-rata: 91
    Predikat: honour

*/

var recapScores = (data) => {
    for(var i = 1; i < data.length ; i++){
        var isinilai = [];
        for(var j = 1; j < data[i].length ; j++){
           isinilai.push(parseInt(data[i][j]));
        }

        var nilai = new Score("", isinilai, "");
        console.log(i+". Email: "+ data[i][0]);
        console.log("Rata-rata: " + nilai.average());
        if(nilai.average() > 90){
            console.log("Predikat: honour");
        }else if(nilai.average() > 80 && nilai.average() <= 90){
            console.log("Predikat: graduate");
        }else if(nilai.average() > 70 && nilai.average() <= 80){
            console.log("Predikat: participant");
        }
        console.log()
    }
}

recapScores(data);