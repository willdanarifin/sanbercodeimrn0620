// No. 1 Looping using While

var i = 1;
console.log("LOOPING PERTAMA")
while (i <= 20) {
    if (i % 2 == 0) {
        console.log(i + " - I love coding");
    }
    i++;
}
console.log("LOOPING KEDUA")
while (i >= 1) {
    if (i % 2 == 0) {
        console.log(i + " - I will become a mobile developer");
    }
    i--;
}

console.log("\n");
// No 2 Looping using for

for (i = 1; i <= 20; i++) {
    if (i % 2 != 0) {
        if (i % 3 == 0) {  
            console.log(i + " - I Love Coding");
        }else{
            console.log(i + " - Santai");
        }

    }else{
        console.log(i + " - Berkualitas");
    }
    
}

console.log("\n");
// No. 3 Membuat Persegi Panjang #

var lebar = 4;
var panjang = 8;

for(i = 0 ; i < lebar ; i++){
    for(var j = 0 ; j < panjang ; j++){
        process.stdout.write("#");
    }
    console.log();
}

console.log("\n");
// No. 4 Membuat Tangga #

var alas = 7;

for(i = 1 ; i <= alas ; i++){
    for(var j = 1 ; j <= i ; j++){
        process.stdout.write("#");
    }
    console.log();
}

console.log("\n");
// No. 5 Membuat Papan Catur #

panjang = 8;
lebar = 8;

for(i = 1 ; i <= lebar ; i++){
    for(var j = 1 ; j <= panjang ; j++){
        if (i % 2 == 0){
            if(j % 2 == 0){
                process.stdout.write("#");
            }else{
                process.stdout.write(" ");
            }
        }else{
            if(j % 2 == 0){
                process.stdout.write(" ");
            }else{
                process.stdout.write("#");
            }
        }
    }
    console.log();
}

