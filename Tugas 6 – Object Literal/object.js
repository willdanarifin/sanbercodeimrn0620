//Soal No. 1 (Array to Object)

function arrayToObject(arr) {
    var sekarang = new Date();
    var tahunini = sekarang.getFullYear();
    var umur = "";
    for (var i = 0; i < arr.length; i++) {
        if (arr[i][3] > tahunini || arr[i][3] == undefined) {
            umur = "Invalid birth year";
        } else {
            umur = tahunini - arr[i][3];
        }
        var personObj = {};
        personObj.firstName = arr[i][0];
        personObj.lastName = arr[i][1];
        personObj.gender = arr[i][2];
        personObj.age = umur;
        console.log((i + 1) + ". " + arr[i][0] + " " + arr[i][1], personObj);
    }
}

var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ];
arrayToObject(people); 

var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ];
arrayToObject(people2);

console.log("\n");
//Soal No. 2 (Shopping Time)
console.log("\n");

function shoppingTime(memberId, money) {

    var products = [
        ["Sepatu brand Stacattu", 1500000], ["Sweater brand Uniklooh", 175000],["Baju brand Zoro", 500000], 
        ["Baju brand H&N", 250000], ["Casing Handphone", 50000],
    ];
    
    products = products.sort(function(a, b) {return b[1] - a[1]});
    
    var changeMoney = 0;
    var boughtProducts = [];
    var cart = {};
    
    
    if (memberId != '' && money != null) {
        if (money >= 50000) {
            changeMoney = money;
            for (var i = 0; i < products.length; i++) {
                if (changeMoney >= products[i][1]) {
                    changeMoney -= products[i][1]
                    boughtProducts.push(products[i][0])
                }
            }
            
            cart.memberId = memberId;
            cart.money = money;
            cart.listPurchased = boughtProducts;
            cart.changeMoney = changeMoney;
            
            return cart; 
        } else {
            return "Mohon maaf, uang tidak cukup";
        }
        
    } else {
        return "Mohon maaf, toko X hanya berlaku untuk member saja";
    }
}
console.log(shoppingTime('1820RzKrnWn08', 2475000));
console.log(shoppingTime('82Ku8Ma742', 170000));
console.log(shoppingTime('', 2475000));
console.log(shoppingTime('234JdhweRxa53', 15000));
console.log(shoppingTime());


console.log("\n");
//Soal No. 3 (Naik Angkot)
console.log("\n");

function naikAngkot(arrPenumpang) {
    var rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    rute = rute.sort(function(a,b) {return a < b ? -1 : 1})

    var output = [];
    var naik = 0;
    var tujuan = 0;
    var indexNaik = 0;
    var indexTujuan = 0;
    var totalPrice = 0;

    if (arrPenumpang !== undefined || arrPenumpang.length != 0) {
        for (var i = 0; i < arrPenumpang.length; i++) {
            naik = arrPenumpang[i][1];
            tujuan = arrPenumpang[i][2];
    
            for (let j = 0; j < rute.length; j++) {
                if (naik == rute[j]) {
                    indexNaik = j + 1;
                }
                if (tujuan == rute[j]) {
                    indexTujuan = j + 1;
                }
            }
            var listAngkot = {};
            totalPrice = (indexTujuan - indexNaik) * 2000;
            listAngkot.penumpang = arrPenumpang[i][0];
            listAngkot.naikDari = naik;
            listAngkot.tujuan = tujuan;
            listAngkot.bayar = totalPrice
            output.push(listAngkot);
        }
    }
    return output
}

console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
console.log(naikAngkot([]))
